name := "web-socket-lobby"

version := "0.1"

scalaVersion := "2.11.11"

libraryDependencies ++= Seq(
  "org.postgresql" % "postgresql" % "42.1.4",
  "com.typesafe.slick" %% "slick" % "3.2.1",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.2.1",
  "com.typesafe.akka" %% "akka-http" % "10.1.1",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.1",
  "com.typesafe.akka" %% "akka-stream" % "2.5.11",
  "com.typesafe.akka" %% "akka-slf4j" % "2.5.11",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
  "org.specs2" %% "specs2-core" % "4.0.1" % Test,
  "com.typesafe.akka" %% "akka-http-testkit" % "10.1.1" % Test,
  "org.eu.acolyte" % "jdbc-scala_2.11" % "1.0.47" % Test
)

parallelExecution in Test := false