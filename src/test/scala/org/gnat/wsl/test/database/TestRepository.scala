package org.gnat.wsl.test.database

import acolyte.jdbc.Implicits._
import acolyte.jdbc.RowLists._
import acolyte.jdbc.{AcolyteDSL, QueryExecution, UpdateExecution, Driver => AcolyteDriver}
import org.gnat.wsl.models.{WslTableRepo, WslUserRepo}
import org.gnat.wsl.test.TestData._
import slick.jdbc.JdbcBackend.Database

trait TestRepository {

  val handler = AcolyteDSL.handleStatement
    .withQueryDetection("^select ")
    .withUpdateHandler { e: UpdateExecution ⇒
      sqlLogger(e)
      // handle delete
      if (e.sql.startsWith("delete ") && e.sql.contains("\"wsl_tables\"")) {
        if (e.sql.matches(".*\"id\" = [1-3]")) 1 else 0
      }
      //// handle insert
      //// acolyte doesn't support prepared statements and slick uses those for INSERT
      //// TODO fix this INSERT issue somehow
      //else if (e.sql.startsWith("insert ") && e.sql.contains("\"wsl_tables\"")) {
      //  UpdateResult.One.withGeneratedKeys(appenderWlsTable(tablesSchema, List(WslTable(name = "some_test_table", participants = 14))))
      //}
      // handle update
      else if (e.sql.startsWith("update ") && e.sql.contains("\"wsl_tables\"")) {
        if (e.sql.contains("\"wsl_tables\"") && e.sql.matches(".*\"id\" = [1-3]")) 1 else 0
      } else 0
    }
    .withQueryHandler { e: QueryExecution ⇒
      sqlLogger(e)
      if (e.sql.contains("\"wsl_users\"") && e.sql.contains("where \"username\"")) {
        val userNameParsed = e.sql.split(" \"username\" = ").last.replaceAll("'", "")
        appenderWlsUsers(usersSchema, usersData.filter(_.username == userNameParsed))
      } else if (e.sql.contains("\"wsl_tables\"") && e.sql.contains("where \"id\"")) {
        val tableIdParsed = Integer.parseInt(e.sql.split(" \"id\" = ").last)
        appenderWlsTable(tablesSchema, tablesData.filter(_.id.contains(tableIdParsed)))
      } else if (e.sql.contains("\"wsl_tables\"") && !e.sql.contains("where")) {
        appenderWlsTable(tablesSchema, tablesData)
      } else rowList1(classOf[String]).asResult
    }

  AcolyteDriver.register("my-handler-id", handler)
  implicit val db = Database.forURL("jdbc:acolyte:anything-you-want?handler=my-handler-id")

  val userRepo = new WslUserRepo
  val tablesRepo = new WslTableRepo
}
