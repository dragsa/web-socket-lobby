package org.gnat.wsl.test.database

import org.gnat.wsl.models.WslUser
import org.gnat.wsl.test.WslBaseSpec
import org.specs2.concurrent.ExecutionEnv

class WslUserRepoSpec(implicit ee: ExecutionEnv) extends WslBaseSpec {
  sequential
  "Wsl User Repository" title

  "select userA by name" in {
    userRepo.getOneByUserName("userA") must beSome(WslUser("userA", "passwordA", "user", 1)).await
  }

  "select userX by name" in {
    userRepo.getOneByUserName("userX") must beNone.await
  }

}
