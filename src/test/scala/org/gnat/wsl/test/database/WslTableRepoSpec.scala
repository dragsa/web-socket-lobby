package org.gnat.wsl.test.database

import org.gnat.wsl.models.WslTable
import org.gnat.wsl.test.WslBaseSpec
import org.specs2.concurrent.ExecutionEnv

class WslTableRepoSpec(implicit ee: ExecutionEnv) extends WslBaseSpec {
  sequential
  "Wsl Table Repository" title

  "select all tables" in {
    tablesRepo.getAll must haveSize[Seq[WslTable]](3).await
  }

  "delete existing table" in {
    tablesRepo.deleteOneById(1) must beEqualTo(1).await
  }

  "delete non-existing table" in {
    tablesRepo.deleteOneById(100) must beEqualTo(0).await
  }

  "update existing table" in {
    tablesRepo.updateOne(WslTable("tableA", 1985, Some(1))) must beEqualTo(1).await
  }

  "update non-existing table" in {
    tablesRepo.updateOne(WslTable("tableZ", 14, Some(100))) must beEqualTo(0).await
  }

}

