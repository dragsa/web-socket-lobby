package org.gnat.wsl.test

import acolyte.jdbc.Implicits._
import acolyte.jdbc.RowLists._
import acolyte.jdbc.{Execution, RowList3, RowList4}
import org.gnat.wsl.models.{WslTable, WslUser}

package object database {

  //    case class WslUser(username: String,
  //                       password: String,
  //                       usertype: String,
  //                       id: Int)

  def appenderWlsUsers(schema: RowList4.Impl[String, String, String, Int], listToAppend: List[WslUser]): RowList4.Impl[String, String, String, Int] = {
    listToAppend match {
      case Nil => schema
      case head :: tail =>
        appenderWlsUsers(
          schema.append(
            head.username,
            head.password,
            head.usertype,
            head.id
          ),
          tail
        )
    }
  }

  val usersSchema = rowList4(
    classOf[String] -> "username",
    classOf[String] -> "password",
    classOf[String] -> "usertype",
    classOf[Int] -> "id"
  )

  //    case class WslTable(name: String,
  //                        participants: Int,
  //                        id: Option[Int] = None)

  def appenderWlsTable(schema: RowList3.Impl[String, Int, Int], listToAppend: List[WslTable]): RowList3.Impl[String, Int, Int] = {
    listToAppend match {
      case Nil => schema
      case head :: tail =>
        appenderWlsTable(
          schema.append(
            head.name,
            head.participants,
            head.id.get
          ),
          tail
        )
    }
  }

  val tablesSchema = rowList3(
    classOf[String] -> "name",
    classOf[Int] -> "participants",
    classOf[Int] -> "id"
  )

  def sqlLogger(e: Execution) = {
    println("SQL: " + e.sql)
    println("Params: " + e.parameters)
  }
}
