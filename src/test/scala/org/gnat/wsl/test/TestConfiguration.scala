package org.gnat.wsl.test

import com.typesafe.config.ConfigFactory

trait TestConfiguration {
  implicit val config = ConfigFactory.load()
}
