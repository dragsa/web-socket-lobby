package org.gnat.wsl.test

import org.gnat.wsl.test.database.TestRepository
import org.specs2.mutable.Specification

trait WslBaseSpec extends Specification with TestRepository with TestConfiguration