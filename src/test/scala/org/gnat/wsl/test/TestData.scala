package org.gnat.wsl.test

import org.gnat.wsl.models.{WslTable, WslUser}

object TestData {
  val usersData = List(
    WslUser("userA", "passwordA", "user", 1),
    WslUser("userB", "passwordB", "admin", 2),
    WslUser("userC", "passwordC", "admin", 3),
    WslUser("userD", "passwordD", "user", 4)
  )

  val tablesData = List(
    WslTable("tableA", 1, Some(1)),
    WslTable("tableB", 14, Some(2)),
    WslTable("tableC", 88, Some(3))
  )
}
