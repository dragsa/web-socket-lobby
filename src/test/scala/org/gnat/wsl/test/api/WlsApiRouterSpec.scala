package org.gnat.wsl.test.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.ws.BinaryMessage
import akka.http.scaladsl.testkit.{RouteTestTimeout, Specs2RouteTest, WSProbe}
import akka.util.ByteString
import org.gnat.wsl.models.WslTable
import org.gnat.wsl.protocol._
import org.gnat.wsl.test.{TestData, WslBaseSpec}
import org.gnat.wsl.{WslApiRouter, WslDatabase}
import org.specs2.specification.Scope
import spray.json._

import scala.concurrent.duration._

class WlsApiRouterSpec extends WslBaseSpec with Specs2RouteTest with WlsRequestJsonSupport with WlsEventJsonSupport {
  sequential
  "Rest Case Analytics Service" title

  def actorRefFactory = ActorSystem()

  implicit def default(implicit system: ActorSystem) = RouteTestTimeout(10.second)

  implicit val dbRef = db
  implicit val database = new WslDatabase
  val restService = new WslApiRouter
  val routes = restService.route

  trait Fixtures extends Scope {
    val wsClient = WSProbe()
  }

  "connect to /api provides Upgrade header" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {
      isWebSocketUpgrade shouldEqual true
      ok
    }
  }

  "correct ping sent results in EventPong" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {

      wsClient.sendMessage(RequestPing(seq = 1).toJson.prettyPrint)
      wsClient.expectMessage(EventPong(seq = 1).toJson.prettyPrint)

      // TODO is not working after migration to Flow.fromSinkAndSource
      //      wsClient.sendCompletion()
      //      wsClient.expectCompletion()

      ok
    }
  }

  "login with proper user/pass results in EventLoginSuccessful" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {

      wsClient.sendMessage(RequestLogin(username = "userA", password = "passwordA").toJson.prettyPrint)
      wsClient.expectMessage(EventLoginSuccess(user_type = "user").toJson.prettyPrint)

      // TODO is not working after migration to Flow.fromSinkAndSource
      //      wsClient.sendCompletion()
      //      wsClient.expectCompletion()

      ok
    }
  }

  "login with bad user/pass results in EventLoginFailed" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {

      wsClient.sendMessage(RequestLogin(username = "userA", password = "passwordXXX").toJson.prettyPrint)
      wsClient.expectMessage(EventLoginFailed().toJson.prettyPrint)

      // TODO is not working after migration to Flow.fromSinkAndSource
      //      wsClient.sendCompletion()
      //      wsClient.expectCompletion()

      ok
    }
  }

  "add new table before login results in EventNotLoggedIn" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {

      wsClient.sendMessage(RequestAddTable(after_id = 100, table = WslTable(name = "some_test_table", participants = 14)).toJson.prettyPrint)
      wsClient.expectMessage(EventNotLoggedIn().toJson.prettyPrint)

      // TODO is not working after migration to Flow.fromSinkAndSource
      //      wsClient.sendCompletion()
      //      wsClient.expectCompletion()

      ok
    }
  }

  "add new table by generic user results in EventNotAuthorized" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {

      wsClient.sendMessage(RequestLogin(username = "userA", password = "passwordA").toJson.prettyPrint)
      wsClient.expectMessage(EventLoginSuccess(user_type = "user").toJson.prettyPrint)

      wsClient.sendMessage(RequestAddTable(after_id = 100, table = WslTable(name = "some_test_table", participants = 14)).toJson.prettyPrint)
      wsClient.expectMessage(EventNotAuthorized().toJson.prettyPrint)

      // TODO is not working after migration to Flow.fromSinkAndSource
      //      wsClient.sendCompletion()
      //      wsClient.expectCompletion()

      ok
    }
  }

  // TODO not clear how to emulate INSERT with acolyte and slick
  //  "add new table by admin user results in that table returned" in new Fixtures {
  //    WS("/api", wsClient.flow) ~> routes ~> check {
  //
  //      wsClient.sendMessage(RequestLogin(username = "userB", password = "passwordB").toJson.prettyPrint)
  //      wsClient.expectMessage(EventLoginSuccess(user_type = "admin").toJson.prettyPrint)
  //
  //      wsClient.sendMessage(RequestAddTable(after_id = 100, table = WslTable(name = "some_test_table", participants = 14)).toJson.prettyPrint)
  //      wsClient.expectMessage(EventTableAdded(after_id = 100, table = WslTable(name = "some_test_table", participants = 14)).toJson.prettyPrint)
  //
  //      wsClient.sendCompletion()
  //      // forcing OnComplete
  //      //        wsClient.expectMessage()
  //      wsClient.expectCompletion()
  //
  //      ok
  //    }
  //  }

  "remove existing table by generic user results in EventNotAuthorized" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {

      wsClient.sendMessage(RequestLogin(username = "userA", password = "passwordA").toJson.prettyPrint)
      wsClient.expectMessage(EventLoginSuccess(user_type = "user").toJson.prettyPrint)

      wsClient.sendMessage(RequestRemoveTable(id = 14).toJson.prettyPrint)
      wsClient.expectMessage(EventNotAuthorized().toJson.prettyPrint)

      // TODO is not working after migration to Flow.fromSinkAndSource
      //      wsClient.sendCompletion()
      //      wsClient.expectCompletion()

      ok
    }
  }

  "remove existing table by admin user results in EventTableRemoved" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {

      wsClient.sendMessage(RequestLogin(username = "userB", password = "passwordB").toJson.prettyPrint)
      wsClient.expectMessage(EventLoginSuccess(user_type = "admin").toJson.prettyPrint)

      wsClient.sendMessage(RequestRemoveTable(id = 2).toJson.prettyPrint)
      wsClient.expectMessage(EventTableRemoved(id = 2).toJson.prettyPrint)

      // TODO is not working after migration to Flow.fromSinkAndSource
      //      wsClient.sendCompletion()
      //      wsClient.expectCompletion()

      ok
    }
  }

  "remove non-existing table by admin user results in EventRemovalFailed" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {

      wsClient.sendMessage(RequestLogin(username = "userB", password = "passwordB").toJson.prettyPrint)
      wsClient.expectMessage(EventLoginSuccess(user_type = "admin").toJson.prettyPrint)

      wsClient.sendMessage(RequestRemoveTable(id = 1000).toJson.prettyPrint)
      wsClient.expectMessage(EventRemovalFailed(id = 1000).toJson.prettyPrint)

      // TODO is not working after migration to Flow.fromSinkAndSource
      //      wsClient.sendCompletion()
      //      wsClient.expectCompletion()

      ok
    }
  }

  "update existing table by generic user results in EventNotAuthorized" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {

      wsClient.sendMessage(RequestLogin(username = "userA", password = "passwordA").toJson.prettyPrint)
      wsClient.expectMessage(EventLoginSuccess(user_type = "user").toJson.prettyPrint)

      wsClient.sendMessage(RequestUpdateTable(table = WslTable(id = Some(2), name = "some_new_tableB_name", participants = 14)).toJson.prettyPrint)
      wsClient.expectMessage(EventNotAuthorized().toJson.prettyPrint)

      // TODO is not working after migration to Flow.fromSinkAndSource
      //      wsClient.sendCompletion()
      //      wsClient.expectCompletion()

      ok
    }
  }

  "update existing table by admin user results in EvenTableUpdated" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {

      wsClient.sendMessage(RequestLogin(username = "userB", password = "passwordB").toJson.prettyPrint)
      wsClient.expectMessage(EventLoginSuccess(user_type = "admin").toJson.prettyPrint)

      wsClient.sendMessage(RequestUpdateTable(table = WslTable(id = Some(2), name = "some_new_tableB_name", participants = 14)).toJson.prettyPrint)
      wsClient.expectMessage(EventTableUpdated(table = WslTable(id = Some(2), name = "some_new_tableB_name", participants = 14)).toJson.prettyPrint)

      // TODO is not working after migration to Flow.fromSinkAndSource
      //      wsClient.sendCompletion()
      //      wsClient.expectCompletion()

      ok
    }
  }

  "update non-existing table by admin user results in EventUpdateFailed" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {

      wsClient.sendMessage(RequestLogin(username = "userB", password = "passwordB").toJson.prettyPrint)
      wsClient.expectMessage(EventLoginSuccess(user_type = "admin").toJson.prettyPrint)

      wsClient.sendMessage(RequestUpdateTable(table = WslTable(id = Some(1000), name = "some_new_tableB_name", participants = 1985)).toJson.prettyPrint)
      wsClient.expectMessage(EventUpdateFailed(id = 1000).toJson.prettyPrint)

      // TODO is not working after migration to Flow.fromSinkAndSource
      //      wsClient.sendCompletion()
      //      wsClient.expectCompletion()

      ok
    }
  }

  "subscribe for tables before login results in EventNotLoggedIn" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {

      wsClient.sendMessage(RequestSubscribe().toJson.prettyPrint)
      wsClient.expectMessage(EventNotLoggedIn().toJson.prettyPrint)

      // TODO is not working after migration to Flow.fromSinkAndSource
      //      wsClient.sendCompletion()
      //      wsClient.expectCompletion()

      ok
    }
  }

  "subscribe for tables results in EventSubscribed" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {

      wsClient.sendMessage(RequestLogin(username = "userB", password = "passwordB").toJson.prettyPrint)
      wsClient.expectMessage(EventLoginSuccess(user_type = "admin").toJson.prettyPrint)

      wsClient.sendMessage(RequestSubscribe().toJson.prettyPrint)
      wsClient.expectMessage(EventSubscribed(tables = TestData.tablesData).toJson.prettyPrint)

      // TODO is not working after migration to Flow.fromSinkAndSource
      //      wsClient.sendCompletion()
      //      wsClient.expectCompletion()

      ok
    }
  }

  "unsubscribe before login results in EventNotLoggedIn" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {

      wsClient.sendMessage(RequestUnsubscribe().toJson.prettyPrint)
      wsClient.expectMessage(EventNotLoggedIn().toJson.prettyPrint)

      ok
    }
  }

  "unsubscribe for tables results in nothing sent in response" in new Fixtures {
    WS("/api", wsClient.flow) ~> routes ~> check {

      wsClient.sendMessage(RequestLogin(username = "userB", password = "passwordB").toJson.prettyPrint)
      wsClient.expectMessage(EventLoginSuccess(user_type = "admin").toJson.prettyPrint)

      wsClient.sendMessage(RequestUnsubscribe().toJson.prettyPrint)
      wsClient.expectNoMessage(500.millis)

      ok
    }
  }

//  TODO add full e2e subscription test
//  "subscribe for tables and listening for updates via other connection result in ES, ETU, ETR" in new Fixtures {
//    WS("/api", wsClient.flow) ~> routes ~> check {
//
//      wsClient.sendMessage(RequestLogin(username = "userB", password = "passwordB").toJson.prettyPrint)
//      wsClient.expectMessage(EventLoginSuccess(user_type = "admin").toJson.prettyPrint)
//
//      wsClient.sendMessage(RequestSubscribe().toJson.prettyPrint)
//      wsClient.expectMessage(EventSubscribed(tables = TestData.tablesData).toJson.prettyPrint)
//
//      ???
//
//      wsClient.sendCompletion()
//      wsClient.expectCompletion()
//
//      ok
//    }
//  }

}
