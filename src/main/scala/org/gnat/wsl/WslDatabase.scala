package org.gnat.wsl

import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import org.gnat.wsl.models._
import slick.jdbc.PostgresProfile.api._
import slick.jdbc.meta.MTable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success, Try}

class WslDatabase(implicit db: Database, config: Config) extends LazyLogging {
  implicit val userRepository = new WslUserRepo
  implicit val tableRepository = new WslTableRepo

  private val tables = Map(
    "wsl_users" -> userRepository.wslUserQuery,
    "wsl_tables" -> tableRepository.wslTableQuery)

  private def initTables {
    tables.keys.foreach(tableCreator)
  }

  private def tableCreator(tableName: String) {
    Try(Await.result(
      db.run(MTable.getTables(tableName))
        .flatMap(matchedTables =>
          if (matchedTables.isEmpty) {
            logger.info(tableName + " table doesn't exist, creating...")
            db.run(tables(tableName).schema.create)
          } else Future.successful())
        .andThen { case _ => logger.info(tableName + " table check finished") },
      config.getConfig("timers").getInt("db-init-timeout ") seconds
    )) match {
      case Success(_) => logger.info("DB init completed")
      case Failure(ex) =>
        logger.error(s"DB init failure happened: $ex")
        System.exit(1)
    }
  }

  def initDatabase {
    logger.info("DB init started")
    initTables
  }
}
