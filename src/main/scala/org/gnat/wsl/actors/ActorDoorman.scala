package org.gnat.wsl.actors

import java.util.UUID

import akka.Done
import akka.actor.{Actor, ActorRef, Props, Status}
import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import org.gnat.wsl.WslDatabase
import org.gnat.wsl.actors.ActorDoorman.{LogActiveUsers, Notification}
import org.gnat.wsl.protocol._
import spray.json._

import scala.collection.mutable.Map
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

class ActorDoorman(implicit db: WslDatabase, config: Config) extends Actor with WlsEventJsonSupport with LazyLogging {

  // [connectionId -> Connection(sender, Option(username, admin?, has subscription?))]
  private var usersLoggedIn = Map[String, Connection]().empty
  private val statsPrintPeriod = config.getInt("timers.stats-print-period")

  override def preStart: Unit = {
    super.preStart()
    logger.info(s"I am ${self.path.name}, alive and well")
    context.system.scheduler.schedule(statsPrintPeriod seconds, statsPrintPeriod seconds, self, LogActiveUsers)
  }

  override def receive: Receive = {

    case LogActiveUsers => prettyPrintLoggedInUser()

    case (connectionKey: String, outActor: ActorRef) =>
      logger.info(s"got remote actor ${outActor.path} from connection $connectionKey")
      // connection and sender are stored, user is not logged in
      usersLoggedIn += (connectionKey -> Connection(outActor, None))

    // domain request processing
    case (connectionKey: String, RequestPing(_, seq)) =>
      logger.info(s"got RP from upstream")
      usersLoggedIn(connectionKey).sender ! EventPong(seq = seq).toJson.prettyPrint

    case (connectionKey: String, rl@RequestLogin(_, username, _)) =>
      logger.info(s"got RL from upstream")
      if (usersLoggedIn(connectionKey).client.isDefined) {
        logger.info(s"there is already logged in user for connection $connectionKey")
        usersLoggedIn(connectionKey).sender ! EventNotAuthorized().toJson.prettyPrint
      } else {
        logger.info(s"user $username tries to login")
        // actor name is connectionKey + random UUID + username for uniqueness and tracking
        context.actorOf(ActorOneTimeDatabaseAccessor.props, connectionKey + "_" + UUID.randomUUID() + "_" + username) ! rl
      }

    case (connectionKey: String, rat: RequestAddTable) => tableRequestProcessor(connectionKey, rat)

    case (connectionKey: String, rut: RequestUpdateTable) => tableRequestProcessor(connectionKey, rut)

    case (connectionKey: String, rrt: RequestRemoveTable) => tableRequestProcessor(connectionKey, rrt)

    case (connectionKey: String, rs: RequestSubscribe) => tableRequestProcessor(connectionKey, rs)

    case (connectionKey: String, _: RequestUnsubscribe) =>
      if (usersLoggedIn(connectionKey).client.isEmpty) {
        usersLoggedIn(connectionKey).sender ! EventNotLoggedIn().toJson.prettyPrint
      } else {
        usersLoggedIn += (connectionKey -> Connection(usersLoggedIn(connectionKey).sender, usersLoggedIn(connectionKey).client.map(_.copy(hasSubscription = false))))
      }

    // domain event processing
    case (actorName: String, els@EventLoginSuccess(_, user_type)) =>
      logger.info(s"got ELS from downstream")
      val tokens = actorName.split("_", 3)
      val (connectionKey, currentSender) = extractConnectionKeyAndSenderLink(actorName)
      logger.debug(s"adding ${tokens.last} to list of logged in users")
      usersLoggedIn += (connectionKey -> Connection(currentSender, Some(WslClient(tokens.last, user_type == "admin", hasSubscription = false))))
      currentSender ! els.toJson.prettyPrint
      prettyPrintLoggedInUser()

    case (actorName: String, es@EventSubscribed(_, _)) =>
      logger.info(s"got ES from downstream")
      val tokens = actorName.split("_", 3)
      val (connectionKey, currentSender) = extractConnectionKeyAndSenderLink(actorName)
      logger.debug(s"adding ${tokens.last} to list of subscribed users")
      usersLoggedIn += (connectionKey -> Connection(currentSender, usersLoggedIn(connectionKey).client.map(_.copy(hasSubscription = true))))
      currentSender ! es.toJson.prettyPrint
      prettyPrintLoggedInUser()

    // events with no special impact on doorman state - just stream back to client
    case (actorName: String, event: Event) => upstreamedEventsProcessor(actorName, event)

    // notification for subscribers
    case (connectionKey: String, event: Event, _: Notification.type) =>
      usersLoggedIn
        .filter { case (key, connection) => (key != connectionKey.toString) && connection.client.flatMap(u => Option(u.hasSubscription)).getOrElse(false) }
        .foreach { case (key, connection) =>
          logger.info(s"notifying connection $key via sender ${connection.sender.path}")
          connection.sender ! (event match {
            case e@EventTableRemoved(_, _) => e.toJson.prettyPrint
            case e@EventTableUpdated(_, _) => e.toJson.prettyPrint
            case e@EventTableAdded(_, _, _) => e.toJson.prettyPrint
          })
        }

    // stream event processing
    case (connectionKey: String, Done) =>
      usersLoggedIn -= connectionKey
      logger.debug(s"upstream completed due to connection closure, user for connection $connectionKey was logged out")
    case (connectionKey: String, Status.Success(ex)) =>
      val error = ex.asInstanceOf[Throwable]
      usersLoggedIn(connectionKey).sender ! {
        if (error.getMessage.contains("request_type_not_supported")) EventRequestTypeNotSupported().toJson.prettyPrint
        else EventMalformedRequest(reason = error.getMessage).toJson.prettyPrint
      }
//      usersLoggedIn -= connectionKey
//      logger.info(s"upstream failed: ${error.getMessage}, user for connection $connectionKey was logged out")
  }

  private def extractConnectionKeyAndSenderLink(actorName: String) = {
    val tokens = actorName.split("_", 3)
    val connectionKey = tokens.head
    (connectionKey, usersLoggedIn(connectionKey).sender)
  }

  private def prettyPrintLoggedInUser() = {
    logger.debug("users active now:\n" + (usersLoggedIn mkString ",\n"))
    logger.debug("users logged in now:\n" + (usersLoggedIn.filter { case (_: String, v: Connection) => v.client.isDefined } mkString ",\n"))
  }

  private def checkAdminPrivileges(connectionKey: String) = {
    (for {maybeUser <- usersLoggedIn(connectionKey).client} yield maybeUser) match {
      case Some(user) => user.isAdmin
      case None => false
    }
  }

  private def tableRequestProcessor(connectionKey: String, request: Request) = {
    if (usersLoggedIn(connectionKey).client.isEmpty) {
      usersLoggedIn(connectionKey).sender ! EventNotLoggedIn().toJson.prettyPrint
    }
    else if (request.$type != "subscribe_tables" && !checkAdminPrivileges(connectionKey)) {
      usersLoggedIn(connectionKey).sender ! EventNotAuthorized().toJson.prettyPrint
    }
    else {
      usersLoggedIn(connectionKey).client.foreach { u =>
        context.actorOf(ActorOneTimeDatabaseAccessor.props,
          connectionKey + "_" + UUID.randomUUID() + "_" + u.name) ! request
      }
    }
  }

  private def upstreamedEventsProcessor(actorName: String, event: Event) = {
    logger.info(s"got event from downstream: $event")
    val (connectionKey, sender) = extractConnectionKeyAndSenderLink(actorName)
    sender ! (event match {
      case e@EventNotAuthorized(_) => e.toJson.prettyPrint
      case e@EventLoginFailed(_) => e.toJson.prettyPrint
      case e@EventNotLoggedIn(_, _) => e.toJson.prettyPrint
      case e@EventThirdPartyError(_, _) => e.toJson.prettyPrint
      case e@EventTableRemoved(_, _) =>
        self ! (connectionKey, e, Notification)
        e.toJson.prettyPrint
      case e@EventRemovalFailed(_, _) => e.toJson.prettyPrint
      case e@EventTableUpdated(_, _) =>
        self ! (connectionKey, e, Notification)
        e.toJson.prettyPrint
      case e@EventUpdateFailed(_, _) => e.toJson.prettyPrint
      case e@EventTableAdded(_, _, _) =>
        self ! (connectionKey, e, Notification)
        e.toJson.prettyPrint
      case _ => EventThirdPartyError(reason = "unknown event received from downstream").toJson.prettyPrint
    })
    prettyPrintLoggedInUser()
  }
}

object ActorDoorman {

  case object LogActiveUsers

  case object Notification

  def props(implicit db: WslDatabase, config: Config) =
    Props(new ActorDoorman)
}
