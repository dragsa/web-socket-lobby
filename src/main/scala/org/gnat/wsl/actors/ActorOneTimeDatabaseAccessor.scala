package org.gnat.wsl.actors

import akka.actor.{Actor, ActorRef, Props}
import com.typesafe.scalalogging.LazyLogging
import org.gnat.wsl.WslDatabase
import org.gnat.wsl.protocol._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

class ActorOneTimeDatabaseAccessor(implicit db: WslDatabase) extends Actor with LazyLogging {

  var currentSender: Option[ActorRef] = None

  override def preStart(): Unit = {
    super.preStart()
    logger.info(s"I am ${self.path.name}, alive and well")
  }

  override def postStop(): Unit = {
    super.postStop()
    logger.info(s"I am ${self.path.name}, witness my demise")
  }

  override def receive: Receive = {
    case rl@RequestLogin(_, username, password) =>
      logger.info(s"got request: $rl")
      currentSender = Some(sender)
      db.userRepository.getOneByUserName(username) onComplete {
        case Success(maybeUser) => maybeUser match {
          case Some(user) =>
            if (password == user.password) {
              logger.info(s"user $username logged in successfully")
              self ! EventLoginSuccess(user_type = user.usertype)
            } else {
              logger.info(s"user $username failed to login, wrong password")
              self ! EventLoginFailed()
            }
          case None =>
            logger.info(s"user $username failed to login, wrong username")
            self ! EventLoginFailed()
        }
        case Failure(ex) =>
          logger.info(s"error happened during table creation: ${ex.printStackTrace()}")
          self ! EventThirdPartyError(reason = ex.getMessage)
      }
    // TODO how to handle this afterId impact?
    case rat@RequestAddTable(_, after_id, wslTable) =>
      logger.info(s"got request: $rat")
      currentSender = Some(sender)
      db.tableRepository.createOne(wslTable) onComplete {
        case Success(newTable) =>
          logger.info(s"table $newTable created")
          self ! EventTableAdded(after_id = after_id, table = newTable)
        case Failure(ex) =>
          logger.info(s"error happened during table creation: ${ex.printStackTrace()}")
          self ! EventThirdPartyError(reason = ex.getMessage)
      }

    case rut@RequestUpdateTable(_, wslTable) =>
      logger.info(s"got request: $rut")
      currentSender = Some(sender)
      (for {currentTable <- db.tableRepository.getOneById(wslTable.id.get)
            updatedTable <- currentTable match {
              case Some(_) => db.tableRepository.updateOne(wslTable)
              case None => Future.successful(0)
            }} yield updatedTable) onComplete {
        case Success(updatedCount) =>
          if (updatedCount == 1) {
            logger.info(s"table $wslTable was updated")
            self ! EventTableUpdated(table = wslTable)
          } else {
            logger.info(s"table $wslTable was not updated because it does not exist")
            self ! EventUpdateFailed(id = wslTable.id.get)
          }
        case Failure(ex) =>
          logger.info(s"error happened during table update: ${ex.printStackTrace()}")
          self ! EventThirdPartyError(reason = ex.getMessage)
      }

    case rut@RequestRemoveTable(_, id) =>
      logger.info(s"got request: $rut")
      currentSender = Some(sender)
      (for {currentTable <- db.tableRepository.getOneById(id)
            updatedTable <- currentTable match {
              case Some(_) => db.tableRepository.deleteOneById(id)
              case None => Future.successful(0)
            }} yield updatedTable) onComplete {
        case Success(removeCount) =>
          if (removeCount > 0) {
            logger.info(s"table with id $id was removed")
            self ! EventTableRemoved(id = id)
          } else {
            logger.info(s"table with id $id was not removed because it does not exist")
            self ! EventRemovalFailed(id = id)
          }
        case Failure(ex) =>
          logger.info(s"error happened during table removal: ${ex.printStackTrace()}")
          self ! EventThirdPartyError(reason = ex.getMessage)
      }

    case rs@RequestSubscribe(_) =>
      logger.info(s"got request: $rs")
      currentSender = Some(sender)
      db.tableRepository.getAll onComplete {
        case Success(tables) =>
          logger.info(s"all tables fetched")
          self ! EventSubscribed(tables = tables.toList)
        case Failure(ex) =>
          logger.info(s"error happened during table creation: ${ex.printStackTrace()}")
          self ! EventThirdPartyError(reason = ex.getMessage)
      }

    case e: Event =>
      currentSender.foreach(_ ! (self.path.name, e))
      context.stop(self)
  }
}

object ActorOneTimeDatabaseAccessor {
  def props(implicit db: WslDatabase) =
    Props(new ActorOneTimeDatabaseAccessor)
}
