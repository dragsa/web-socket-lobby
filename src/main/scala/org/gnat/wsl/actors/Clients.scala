package org.gnat.wsl.actors

import akka.actor.ActorRef

case class WslClient(name: String, isAdmin: Boolean, hasSubscription: Boolean)

case class Connection(sender: ActorRef, client: Option[WslClient])
