package org.gnat.wsl

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import slick.jdbc.PostgresProfile.api._

import scala.io.StdIn

object WslServer extends App with LazyLogging {
  implicit val system = ActorSystem("wsl-server-system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  implicit val config = ConfigFactory.load()
  implicit val dbRef = Database.forConfig("db")
  implicit val db = new WslDatabase

  val apiRouter = new WslApiRouter

  val serverConfig = config.getConfig("server")
  val host = serverConfig.getString("host")
  val port = serverConfig.getInt("port")

  val bindingFuture = Http().bindAndHandle(apiRouter.route, host, port)

  logger.info("started WSL server, press enter to stop")
  StdIn.readLine()
  bindingFuture
    .flatMap(_.unbind())
    .onComplete(_ => system.terminate())
  logger.info("stopping WSL server")
}
