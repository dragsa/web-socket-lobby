package org.gnat.wsl.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.gnat.wsl.models.{WslTable, WslUser}
import spray.json.DefaultJsonProtocol

trait WslModelJsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val wslTableFormat = jsonFormat3(WslTable)
  implicit val wslUserFormat = jsonFormat4(WslUser)
}

trait WlsRequestJsonSupport extends SprayJsonSupport with DefaultJsonProtocol with WslModelJsonSupport {
  implicit val reqImpl = jsonFormat1(RequestImpl)
  implicit val reqLogin = jsonFormat3(RequestLogin)
  implicit val reqPing = jsonFormat2(RequestPing)
  implicit val reqSubscribe = jsonFormat1(RequestSubscribe)
  implicit val reqUnsubscribe = jsonFormat1(RequestUnsubscribe)
  implicit val reqAddTable = jsonFormat3(RequestAddTable)
  implicit val reqUpdateTable = jsonFormat2(RequestUpdateTable)
  implicit val reqRemoveTable = jsonFormat2(RequestRemoveTable)
}

trait WlsEventJsonSupport extends SprayJsonSupport with DefaultJsonProtocol with WslModelJsonSupport {
  implicit val evtMalformedReq = jsonFormat2(EventMalformedRequest)
  implicit val evtNotSupported = jsonFormat1(EventRequestTypeNotSupported)
  implicit val evtThirdPartyError = jsonFormat2(EventThirdPartyError)
  implicit val evtNotAuthorized = jsonFormat1(EventNotAuthorized)
  implicit val evtNotLoggedIn = jsonFormat2(EventNotLoggedIn)
  implicit val evtLoginSuccess = jsonFormat2(EventLoginSuccess)
  implicit val evtLoginFailed = jsonFormat1(EventLoginFailed)
  implicit val evtPong = jsonFormat2(EventPong)
  implicit val evtSubscribed = jsonFormat2(EventSubscribed)
  implicit val evtTableAdded = jsonFormat3(EventTableAdded)
  implicit val evtTableUpdated = jsonFormat2(EventTableUpdated)
  implicit val evtUpdateFailed = jsonFormat2(EventUpdateFailed)
  implicit val evtTableRemoved = jsonFormat2(EventTableRemoved)
  implicit val evtRemovalFailed = jsonFormat2(EventRemovalFailed)
}