package org.gnat.wsl.protocol

import org.gnat.wsl.models.WslTable

sealed trait Request {
  val $type: String
}

case class RequestImpl($type: String) extends Request

case class RequestLogin($type: String = RequestCodes.login, username: String, password: String) extends Request

case class RequestPing($type: String = RequestCodes.ping, seq: Int) extends Request

case class RequestSubscribe($type: String = RequestCodes.subscribe) extends Request

case class RequestUnsubscribe($type: String = RequestCodes.unsubscribe) extends Request

case class RequestAddTable($type: String = RequestCodes.addTable, after_id: Int, table: WslTable) extends Request

case class RequestUpdateTable($type: String = RequestCodes.updateTable, table: WslTable) extends Request

case class RequestRemoveTable($type: String = RequestCodes.removeTable, id: Int) extends Request

object RequestCodes {
  val login = "login"
  val ping = "ping"
  val subscribe = "subscribe_tables"
  val unsubscribe = "unsubscribe_tables"
  val addTable = "add_table"
  val updateTable = "update_table"
  val removeTable = "remove_table"
}

