package org.gnat.wsl.protocol

import org.gnat.wsl.models.WslTable

trait Event {
  val $type: String
}

case class EventMalformedRequest($type: String = ErrorCodes.malformedRequest, reason: String) extends Event

case class EventThirdPartyError($type: String = ErrorCodes.thirdPartyError, reason: String) extends Event

case class EventRequestTypeNotSupported($type: String = ErrorCodes.notSupported) extends Event

case class EventLoginSuccess($type: String = EventCodes.loginSuccessful, user_type: String) extends Event

case class EventLoginFailed($type: String = ErrorCodes.loginFailed) extends Event

case class EventPong($type: String = EventCodes.pong, seq: Int) extends Event

case class EventSubscribed($type: String = EventCodes.tableList, tables: List[WslTable]) extends Event

case class EventNotAuthorized($type: String = ErrorCodes.notAuthorized) extends Event

case class EventNotLoggedIn($type: String = ErrorCodes.notLoggedIn, reason: String = "the one should login first") extends Event

case class EventTableAdded($type: String = EventCodes.tableAdded, after_id: Int, table: WslTable) extends Event

case class EventTableUpdated($type: String = EventCodes.tableUpdated, table: WslTable) extends Event

case class EventUpdateFailed($type: String = ErrorCodes.updateFailed, id: Int) extends Event

case class EventTableRemoved($type: String = EventCodes.tableRemoved, id: Int) extends Event

case class EventRemovalFailed($type: String = ErrorCodes.removalFailed, id: Int) extends Event

// localize possible response codes in one place

object ErrorCodes {
  // additional response codes not in scope of task, but convenient to use
  val malformedRequest = "malformed_request"
  val notLoggedIn = "not_logged_in"
  val thirdPartyError = "third_party_error"
  val notSupported = "request_type_not_supported"
  val loginFailed = "login_failed"
  val notAuthorized = "not_authorized"
  val updateFailed = "update_failed"
  val removalFailed = "removal_failed"
}

object EventCodes {
  val loginSuccessful = "login_successful"
  val pong = "pong"
  val tableList = "table_list"
  val tableAdded = "table_added"
  val tableUpdated = "table_updated"
  val tableRemoved = "table_removed"
}
