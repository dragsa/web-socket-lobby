package org.gnat.wsl

package object models {

  case class WslUser(username: String,
                     password: String,
                     usertype: String,
                     id: Int)

  case class WslTable(name: String,
                      participants: Int,
                      id: Option[Int] = None)

}
