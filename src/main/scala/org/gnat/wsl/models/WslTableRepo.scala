package org.gnat.wsl.models

import slick.jdbc.PostgresProfile.api._
import slick.lifted.Tag

import scala.concurrent.Future

class WslTableTable(tag: Tag) extends Table[WslTable](tag, "wsl_tables") {

  def name = column[String]("name", O.Length(50), O.Unique)

  def participants = column[Int]("participants")

  def id = column[Int]("id", O.PrimaryKey, O.Unique, O.AutoInc)

  def * =
    (name, participants, id.?) <> (WslTable.apply _ tupled, WslTable.unapply)

}

object WslTableTable {
  val table = TableQuery[WslTableTable]
}

class WslTableRepo(implicit db: Database) {
  val wslTableQuery = WslTableTable.table

  def createOne(table: WslTable): Future[WslTable] = {
    db.run(wslTableQuery returning wslTableQuery += table)
  }

//  def createMany(tables: List[WslTable]): Future[Seq[WslTable]] = {
//    db.run(wslTableQuery returning wslTableQuery ++= tables)
//  }

  def updateOne(table: WslTable): Future[Int] = {
    db.run(
      wslTableQuery
        .filter(_.id === table.id)
        .update(table))
  }

  def getOneById(id: Int): Future[Option[WslTable]] = {
    db.run(wslTableQuery.filter(_.id === id).result.headOption)
  }

  def getOneByName(name: String): Future[Option[WslTable]] = {
    db.run(wslTableQuery.filter(_.name === name).result.headOption)
  }

  def getAll: Future[Seq[WslTable]] = {
    db.run(wslTableQuery.result)
  }

  def deleteOneById(id: Int): Future[Int] = {
    db.run(wslTableQuery.filter(_.id === id).delete)
  }
}
