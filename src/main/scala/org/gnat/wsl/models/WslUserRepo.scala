package org.gnat.wsl.models

import slick.jdbc.PostgresProfile.api._
import slick.lifted.Tag

import scala.concurrent.Future

class WslUserTable(tag: Tag) extends Table[WslUser](tag, "wsl_users") {

  def username = column[String]("username", O.Length(50), O.Unique)

  def password = column[String]("password", O.Length(50))

  def usertype = column[String]("usertype", O.Length(5))

  def id = column[Int]("id", O.PrimaryKey, O.Unique, O.AutoInc)

  def * =
    (username, password, usertype, id) <> (WslUser.apply _ tupled, WslUser.unapply)

}

object WslUserTable {
  val table = TableQuery[WslUserTable]
}

class WslUserRepo(implicit db: Database) {
  val wslUserQuery = WslUserTable.table

//  def createOne(user: WslUser): Future[WslUser] = {
//    db.run(wslUserQuery returning wslUserQuery += user)
//  }
//
//  def createMany(users: List[WslUser]): Future[Seq[WslUser]] = {
//    db.run(wslUserQuery returning wslUserQuery ++= users)
//  }
//
//  def updateOne(user: WslUser): Future[Int] = {
//    db.run(
//      wslUserQuery
//        .filter(_.id === user.id)
//        .update(user))
//  }
//
//  def getOneById(id: Int): Future[Option[WslUser]] = {
//    db.run(wslUserQuery.filter(_.id === id).result.headOption)
//  }

  def getOneByUserName(username: String): Future[Option[WslUser]] = {
    db.run(wslUserQuery.filter(_.username === username).result.headOption)
  }

  def getAll: Future[Seq[WslUser]] = {
    db.run(wslUserQuery.result)
  }
}
