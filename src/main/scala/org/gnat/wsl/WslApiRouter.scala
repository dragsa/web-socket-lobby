package org.gnat.wsl

import java.util.UUID

import akka.actor.{ActorSystem, Status}
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.http.scaladsl.server.Directives._
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.util.Timeout
import akka.{Done, NotUsed}
import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import org.gnat.wsl.actors.ActorDoorman
import org.gnat.wsl.protocol._
import spray.json._

import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

class WslApiRouter(implicit config: Config, system: ActorSystem, database: WslDatabase) extends LazyLogging with WlsEventJsonSupport with WlsRequestJsonSupport {

  val route =
    pathPrefix("api") {
      pathEndOrSingleSlash {
        logger.info(s"connection request received")
        val connectionKey = UUID.randomUUID().toString
        logger.debug(s"socket key is $connectionKey")
        handleWebSocketMessages(apiHandler(connectionKey))
      }
    }

  val actorDoorman = system.actorOf(ActorDoorman.props(database, config), "doorman")

  implicit val askTimeout = Timeout(10 seconds)

  private def apiHandler(connectionKey: String): Flow[Message, Message, _] = {

    val incomingMessages: Sink[Message, NotUsed] = Flow[Message].collect {
      case TextMessage.Strict(txt) =>
        (requestConverter[RequestImpl](txt), txt)
    }
      .collect {
        case (Success(ri@RequestImpl(reqType)), txt) =>
          logger.info(s"request parsed: $ri")
          (reqType, txt)
        case (Failure(_), _) => ("bad request", "")
      }
      .map {
        case ("ping", txt) => requestConverter[RequestPing](txt)
        case ("login", txt) => requestConverter[RequestLogin](txt)
        case ("add_table", txt) => requestConverter[RequestAddTable](txt)
        case ("update_table", txt) => requestConverter[RequestUpdateTable](txt)
        case ("remove_table", txt) => requestConverter[RequestRemoveTable](txt)
        case ("subscribe_tables", txt) => requestConverter[RequestSubscribe](txt)
        case ("unsubscribe_tables", txt) => requestConverter[RequestUnsubscribe](txt)
        case (req, _) =>
          logger.info(s"unknown request type received: $req")
          Failure(new Exception(ErrorCodes.notSupported))
      }
      .map {
        case Success(req) =>
          logger.info(s"in request handler for connection: $connectionKey")
          actorDoorman ! (connectionKey, req)
        case Failure(ex) =>
          logger.error(s"error occurred for connection $connectionKey during request parsing")
          actorDoorman ! (connectionKey, Status.Success(ex))
      }
      .recover {
        case ex =>
          logger.error(s"unexpected error occurred for connection $connectionKey:\n${ex.getMessage}")
          actorDoorman ! (connectionKey, Status.Failure(ex))
      }.to(Sink.actorRef(actorDoorman, (connectionKey, Done)))

    val outgoingMessages: Source[Message, NotUsed] =
      Source.actorRef(10, OverflowStrategy.fail)
        .mapMaterializedValue { outActor =>
          actorDoorman ! (connectionKey, outActor)
          NotUsed
        } map {
        e: String => TextMessage(e)
      }
    Flow.fromSinkAndSource[Message, Message](incomingMessages, outgoingMessages)
  }

  private def requestConverter[T <: Request : JsonReader](stringEntity: String) = {
    logger.info(s"parsing $stringEntity")
    Try(stringEntity.parseJson.convertTo[T])
  }
}
